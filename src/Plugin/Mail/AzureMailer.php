<?php

namespace Drupal\azure_mailer\Plugin\Mail;

use Mobomo\AzureHmacAuth\AzureHMACMiddleware;
use Drupal\Core\Mail\MailInterface;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Render\Markup;

/**
 * A Drupal mail backend to support Azure Communication Services.
 *
 * @Mail(
 *   id = "azure_mailer",
 *   label = @Translation("Azure Communication Service"),
 *   description = @Translation("Sends emails through the Azure Communication Service")
 * )
 */
class AzureMailer implements MailInterface {

  /**
   * @inheritdoc
   */
  public function format(array $message) {
    // ...
    return $message;
  }

  /**
   * @inheritdoc
   */
  public function mail(array $message) {
    $config = \Drupal::config('azure_mailer.settings');
    $endpoint = $config->get('endpoint');
    $secret = $config->get('secret');

    $azureMessage = [
      'recipients' => [
        'to' => [
          [
            'address' => $message['to'],
          ],
        ],
      ],
      'senderAddress' => $message['from'],
      //'attachments' => [],
      'headers' => $message['headers'],
      'replyTo' => [
        [
          'address' => $message['reply-to'],
        ],
      ],
    ];

    if ($message['body'] instanceof MarkupInterface) {
      $azureMessage['content'] = [
        'html' => $message['body'],
        'plainText' => strip_tags($message['body']),
        'subject' => $message['subject'],
      ];
    }
    else {
      $azureMessage['content'] = [
        'html' => '<html><head><title>' .
            $message['subject'] .
          '</title></head><body>' . 
            str_replace("\n", "\n<br/>", $message['body'][0]) .
          '</body></html>',
        'plainText' => $message['body'][0],
        'subject' => $message['subject'],
      ];
    }

    $serializedBody = json_encode($azureMessage);

    $azureHMACMiddleware = new AzureHMACMiddleware($secret);

    $handlerStack = HandlerStack::create();
    $handlerStack->push($azureHMACMiddleware, 'hmac-auth');

    try {
      $client = new Client([
        'handler' => $handlerStack,
      ]);
      $requestMessage = new Request(
          'POST',
          'https://' . $endpoint . '/emails:send?api-version=2023-03-31',
          array(
              'Content-Type' => 'application/json',
          ),
          $serializedBody
      );
      
      $client->send($requestMessage);
    }
    catch(\Exception $e) {
      \Drupal::logger('azure_mailer')->error(
        'Azure Communication Services error: ' . $e->getMessage()
      );
      return FALSE;
    }
    return TRUE;
  }

}
